import os
import asyncio
import random
import discord
from discord.ext import commands


bot = commands.Bot(command_prefix='!', description='abcdefhijklmnopqrstuvwxyz')


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

# !MESSAGECOUNT
# HOW MANY MESSAGES YOU'VE SENT
@bot.command(pass_context=True)
async def messagecount(context):
    counter = 0
    tmp = await bot.send_message(context.message.channel, 'Calculating messages...')
    async for log in bot.logs_from(context.message.channel, limit=100):
        if log.author == context.message.author:
            counter += 1
    await bot.edit_message(tmp, 'You have {} messages.'.format(counter))


# !OVERWATCH [*PLAYERS]
# RANDOM OVERWATCH CHARACTERS
@bot.command(pass_context=True)
async def overwatch(context, *args):

    #List heroes and shuffle them
    heroes = ['Soldier 76', 'Hanzo', 'Lucio', 'Genji', 'Mei', 'Ana', 'Winston', 'Pharah', 'Sombra', 'Tracer', 'Mercy', 'Reinhardt', 'Bastion', 'Zenyatta', 'Junkrat', 'Mccree', 'Roadhog', 'Widowmaker', 'Zarya', 'Torbjorn', 'Orisa', 'Reaper', 'Symmetra', 'D.Va']
    random.shuffle(heroes)

    output = ''

    if len(args) > 24:
        await bot.send_message(context.message.channel, 'Nope.')

    elif len(args):
        for i in range(len(args)):
            output = output + '%s: %s\n' % (args[i], heroes[i])
    else:
        output = 'Random hero: %s' % (heroes[0])

    await bot.send_message(context.message.channel, output)



# !OVERWATCH [*PLAYERS]
# RANDOM OVERWATCH CHARACTERS
@bot.command(pass_context=True)
async def rounds(context, *players):

    #List heroes and shuffle them
    heroes = ['Soldier: 76', 'Hanzo', 'Lucio', 'Genji', 'Mei', 'Ana', 'Winston', 'Pharah', 'Sombra', 'Tracer', 'Mercy', 'Reinhardt', 'Bastion', 'Zenyatta', 'Junkrat', 'Mcree', 'Roadhog', 'Widowmaker', 'Zarya', 'Torbjorn', 'Orisa', 'Reaper', 'Symmetra', 'D.Va']
    random.shuffle(heroes)

    output = ''

    if len(players) > 5:
        await bot.send_message(context.message.channel, 'Too many players, not enough heroes!')
    else:
        for x in range(1, 6):
            output = output + 'Round %s:\n' % (x)

            for i in range(len(players)):
                output = output + '%s: %s\n' % (players[i], heroes[x + (i*5)])

            output = output + '\n'

        await bot.send_message(context.message.channel, output)




@bot.command(pass_context=True)
async def suck(context, *item):
    await bot.send_message(context.message.channel, 'Sucking %s' % (' '.join(item)))


@bot.command(pass_context=True)
async def dankteam(context, *players):
    await bot.send_message(context.message.channel, 'Round x:\nCraig: Widowmaker\nAndy: Hanzo\nAlex: Genji')


@bot.command(pass_context=True)
async def tryhard(context, *players):
    await bot.send_message(context.message.channel, 'Round x:\nCraig: Zenyatta\nAndy: Mercy\nAlex: Ana')

bot.run(os.environ.get('DISCORD_TOKEN'))